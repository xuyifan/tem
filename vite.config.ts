import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import legacy from '@vitejs/plugin-legacy'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    legacy({
      targets: ['defaults', 'ie >= 11', 'chrome 49'],
      // 针对传统浏览器的额外polyfills，之所有有这个字段，是因为 plugin-legacy内部只包含了core-js相关的polyfills，如果开发者希望添加非corejs的polyfill，就加在这个字段里面
      additionalLegacyPolyfills: ['regenerator-runtime/runtime'],
      // 是否编译传统代码。true的话会编译一份额外的针对传统浏览器(不支持esm的浏览器)的代码。
      renderLegacyChunks: true,
      // 默认情况下，polyfills 块是根据目标浏览器范围和最终包中的实际使用情况生成的（通过@babel/preset-env's检测到useBuiltIns: 'usage'）针对具有本机 ESM 不支持的浏览器）。
      // 为字符串列表以明确控制要包含哪些 polyfill 建议配置按需引入
      polyfills:true,
      // 默认为false. 启用此选项将为现代构建生成一个单独的 polyfills 块（针对具有本机 ESM 支持的浏览器）。
      // 设置为字符串列表以明确控制要包含哪些 polyfill - 建议配置按需引入
      modernPolyfills: true
      // polyfills: [
      //   'es.symbol',
      //   'es.array.filter',
      //   'es.promise',
      //   'es.promise.finally',
      //   'es/map',
      //   'es/set',
      //   'es.array.for-each',
      //   'es.object.define-properties',
      //   'es.object.define-property',
      //   'es.object.get-own-property-descriptor',
      //   'es.object.get-own-property-descriptors',
      //   'es.object.keys',
      //   'es.object.to-string',
      //   'web.dom-collections.for-each',
      //   'esnext.global-this',
      //   'esnext.string.match-all'
      // ]
    }) 
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  build:{
    target:['es2015', 'chrome49'],
    cssTarget:'chrome49'
  },
  server:{
    proxy: {
      "/api": {
        target: "http://10.8.0.14:8880/",
        // target: `http://192.168.71.157:18005/`,
        // target: `http://44.53.6.121:18001/`,
        changeOrigin: true,
      },
    },
  }
})

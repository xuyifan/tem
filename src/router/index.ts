import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home/index.vue'),
    },
    {
      path: "/index",
      redirect: () => {
        return { path: "/" };
      },
    },
    {
      // 👇 非严格匹配，都指向 SubApps 页面
      path: '/sub-apps/:page*',
      name: 'sub-apps',
      component: () => import('@/views/SubApps/index.vue'),
    },
  ],
})

export default router

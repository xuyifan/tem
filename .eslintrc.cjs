/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-typescript",
    "@vue/eslint-config-prettier",
  ],
  parserOptions: {
    ecmaVersion: "latest",
  },
  rules: {
    "vue/multi-word-component-names": [
      2,
      {
        ignores: ["index"], //需要忽略的组件名
      },
    ],
    "semi": 0,
    "wrapAttributes":0,
    "prettier/prettier":0,
    "bracketSpacing": 0,
  },
}